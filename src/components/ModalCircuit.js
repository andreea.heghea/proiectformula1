import React, {useState, useEffect} from "react";
import { onValue} from "firebase/database";
import { ref } from "firebase/database";
import "../css/ModalCircuit.css";
import { db } from "../firebase/firebase";

function ModalCircuit ({closeModalCircuit, selectedCircuit}) {
  const [circuits, setCircuits] = useState([]);
  const [imageUrls, setImageUrls] = useState([]);
  const [imageNames, setImageNames] = useState([]);
  

  useEffect(() => {
    const countRef = ref(db, "race/");
    onValue(countRef, (snapshot) => {
      const data = snapshot.val();
      if (data) {
        const newPosts = Object.keys(data).map((key) => ({
          id: key,
          ...data[key],
        }));
        setCircuits(newPosts);
      } else {
        setCircuits([]);
      }
    });
    
  }, []);

  return (
    <div className="modal_background">
      <div className="modal_container">
        <div className="title_closebutton">
          <button onClick={() => closeModalCircuit(false)}> X </button>
        </div>
          {selectedCircuit && (
          <div>
            <li key={selectedCircuit.gpname}>
            <div className="circuit_name">{selectedCircuit.gpname}</div>
            <div className="circuit_info_container">
              <div className="circuit_info_modal">
                <div className="label">Grand Prix year</div>
                <div className="value">{selectedCircuit.gpyear}</div>
              </div>
              <div className="circuit_info_modal">
                <div className="label">Circuit name</div>
                <div className="value">{selectedCircuit.circuitName}</div>
              </div>
              <div className="circuit_info_modal">
                <div className="label">No. of laps</div>
                <div className="value">{selectedCircuit.nolaps}</div>
              </div>
              <div className="circuit_info_modal">
                <div className="label">Circuit length</div>
                <div className="value">{selectedCircuit.circuitlength}</div>
              </div>
              <div className="circuit_info_modal">
                <div className="label">Race distance</div>
                <div className="value">{selectedCircuit.racedistance}</div>
              </div>
              <div className="circuit_info_modal">
                <div className="label">Lap record</div>
                <div className="value">{selectedCircuit.laprecord}</div>
              </div>
              <div className="circuit_info_modal">
                <div className="label">Lap record owner</div>
                <div className="value">{selectedCircuit.laprecordowner}</div>
              </div>
              <div className="circuit_info_modal">
                <div className="label">Lap record year</div>
                <div className="value">{selectedCircuit.laprecordyear}</div>
              </div>
              <div className="circuit_photo_container">
                <img className="circuit_photo" src={selectedCircuit.imageUrls} />
              </div>
            </div>      
          </li>
      </div>
        )}
      </div>
    </div>

  );

}

export default ModalCircuit;