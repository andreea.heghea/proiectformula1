import React, {useState, useEffect} from "react";
import { onValue} from "firebase/database";
import { ref } from "firebase/database";
import "../css/ModalDriver.css";
import { db } from "../firebase/firebase";

function ModalDriver ({closeModalDriver, selectedDriver}) {
  const [drivers, setDrivers] = useState([]);

  useEffect(() => {
    const countRef = ref(db, "driver/");
    onValue(countRef, (snapshot) => {
      const data = snapshot.val();
      if (data) {
        const newPosts = Object.keys(data).map((key) => ({
          id: key,
          ...data[key],
        }));
        setDrivers(newPosts);
      } else {
        setDrivers([]);
      }
    });
    
  }, []);

  return (
    <div className="modal_background">
      <div className="modal_container">
        <div className="title_closebutton">
          <button onClick={() => closeModalDriver(false)}> X </button>
        </div>
          {selectedDriver && (
          <div>
            <li key={selectedDriver.drivername}>
            <div className="driver_name">{selectedDriver.drivername}</div>
            <div className="driver_info_container">
              <div className="driver_info_modal">
                <div className="label">Team</div>
                <div className="value">{selectedDriver.team}</div>
              </div>
              <div className="driver_info_modal">
                <div className="label">Racing number</div>
                <div className="value">{selectedDriver.racingnumber}</div>
              </div>
              <div className="driver_info_modal">
                <div className="label">Country</div>
                <div className="value">{selectedDriver.country}</div>
              </div>
              <div className="driver_info_modal">
                <div className="label">Podiums</div>
                <div className="value">{selectedDriver.podiums}</div>
              </div>
              <div className="driver_info_modal">
                <div className="label">Points</div>
                <div className="value">{selectedDriver.points}</div>
              </div>
              <div className="driver_info_modal">
                <div className="label">Grand Prix enter</div>
                <div className="value">{selectedDriver.gpenter}</div>
              </div>
              <div className="driver_info_modal">
                <div className="label">World Championships</div>
                <div className="value">{selectedDriver.wc}</div>
              </div>
              <div className="driver_info_modal">
                <div className="label">Highest race finish</div>
                <div className="value">{selectedDriver.highestracefinish}</div>
              </div>
              <div className="driver_info_modal">
                <div className="label">Highest grid position</div>
                <div className="value">{selectedDriver.highestgridposition}</div>
              </div>
              <div className="driver_info_modal">
                <div className="label">Date of birth</div>
                <div className="value">{selectedDriver.datebirth}</div>
              </div>
              <div className="driver_info_modal">
                <div className="label">Place of birth</div>
                <div className="value">{selectedDriver.placebirth}</div>
              </div>
              <div className="driver_photo_container">
                <img className="driver_photo" src={selectedDriver.imageUrls} />
              </div>
            </div>      
          </li>
      </div>
        )}
      </div>
    </div>

  );

}

export default ModalDriver;