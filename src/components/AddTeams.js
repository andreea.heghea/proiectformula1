import React, { useState } from "react";
import { ref, push} from "firebase/database";
import {ref as sRef} from "firebase/storage";
import { db } from "../firebase/firebase";
import { uploadBytes } from "firebase/storage";
import { storage } from "../firebase/firebase";
import { getDownloadURL } from "firebase/storage";
import { v4 } from "uuid";
import "../css/AddTeams.css";

export default function AddTeams() {
  const [teamname, setTeamname] = useState("");
  const [base, setBase] = useState("");
  const [principalchief, setPrincipalchief] = useState("");
  const [technicalchief, setTechnicalchief] = useState("");
  const [chassis, setChassis] = useState("");
  const [powerunit, setPowerunit] = useState("");
  const [firstentry, setFirstEntry] = useState("");
  const [wcc, setWcc] = useState("");
  const [highestfinish, setHighestfinish] = useState("");
  const [poleposition, setPoleposition] = useState("");
  const [fastestlap, setFastestlap] = useState("");
  const [teamPictureupload, setTeamPictureupload] = useState("");
  const [uid, setUid]= useState("");
  const [teamImageUpload, setTeamImageUpload] = useState(null);
  const [teamImageUrls, setTeamImageUrls] = useState([]);
  const [teamnameError, setTeamnameError] = useState(false);
  const [baseError, setBaseError] = useState(false);
  const [principalchiefError, setPrincipalchiefError] = useState(false);
  const [technicalchiefError, setTechnicalchiefError] = useState(false);
  const [chassisError, setChassisError] = useState(false);
  const [powerunitError, setPowerunitError] = useState(false);
  const [firstentryError, setFirstEntryError] = useState(false);
  const [wccError, setWcError] = useState(false);
  const [highestfinishError, setHighestfinishError] = useState(false);
  const [polepositionError, setPolepositionError] = useState(false);
  const [fastestlapError, setFastestlapError] = useState(false);
  const [teamPictureuploadError, setTeamPictureuploadError] = useState(false);


  const handleSubmit = (e) => {
    const uid=v4();
    e.preventDefault();
    const newTeam = {
      teamname : teamname,
      base : base,
      principalchief : principalchief,
      technicalchief : technicalchief,
      chassis : chassis,
      powerunit : powerunit,
      firstentry : firstentry,
      wcc : wcc,
      highestfinish : highestfinish,
      poleposition : poleposition,
      fastestlap : fastestlap,
      teamPictureupload : teamPictureupload,
      uid : uid
    };

    const teamRef = ref(db, "team");
    push(teamRef, newTeam)
    .then(() => {
      console.log("Team added successfully!");
      setTeamname("");
      setBase("");
      setPrincipalchief("");
      setTechnicalchief("");
      setChassis("");
      setPowerunit("");
      setFirstEntry("");
      setWcc("");
      setHighestfinish("");
      setPoleposition("");
      setFastestlap("");
      setTeamPictureupload("");
      setUid("");
    })
    .catch((error) => {
      if(!teamname) {
        setTeamnameError("Team name cannot be null");
      } 
      else setTeamnameError(""); 
      if(!base) {
        setBaseError("Team base cannot be null");
      } 
      else setBaseError(""); 
      if(!principalchief) {
        setPrincipalchiefError("Team principal cannot be null");
      } 
      else setPrincipalchiefError(""); 
      if(!technicalchief) {
        setTechnicalchiefError("Team technical chief cannot be null");
      } 
      else setTechnicalchiefError(""); 
      if(!chassis) {
        setChassisError("Chassis cannot be null");
      } 
      else setChassisError(""); 
      if(!powerunit) {
        setPowerunitError("Power unit cannot be null");
      } 
      else setPowerunitError(""); 
      if(!firstentry) {
        setFirstEntryError("First entry cannot be null");
      } 
      else setFirstEntryError("");
      if(firstentry < 1950){
        setFirstEntryError("Year must be higher than 1950")
      }
      if(!wcc) {
        setWcError("World championship cannot be null");
      } 
      else setWcError(""); 
      if(wcc < 0 ){
        setWcError("World championship must be positive");
      }
      if(!highestfinish) {
        setHighestfinishError("Highest finishcannot be null");
      } 
      else setHighestfinishError("");
      if(highestfinish < 0  || highestfinish > 20){
        setHighestfinishError("Highest race finish must be greater than 0 but less than 20");
      }
      if(!poleposition) {
        setPolepositionError("Pole position cannot be null");
      } 
      else setPolepositionError("");
      if(poleposition < 0) {
        setPolepositionError("Pole position number must be positive")
      }
      if(!fastestlap) {
        setFastestlapError("Fastest lap cannot be null");
      } 
      else setFastestlapError(""); 
      if(!teamPictureupload) {
        setTeamPictureuploadError("Picture upload field cannot be null");
      } 
      else setTeamPictureuploadError(""); 
    });

    if (teamImageUpload == null) return;
    const imageRef = sRef(storage, `images/team/${uid}`);
    uploadBytes(imageRef, teamImageUpload).then((snapshot) => {
      getDownloadURL(snapshot.ref).then((url) => {
        setTeamImageUrls((prev) => [...prev, url]);
      });
    });
  }

  return(
  <div className="add_teams">
    <form onSubmit={handleSubmit}>
      <h1>Add Team</h1>
      <input
      type="text"
      className="form_input"
      value={teamname}
      onChange={(e) => setTeamname(e.target.value)}
      placeholder="Enter team name"
      ></input>
      {teamnameError && <div className="error_adddriver">{teamnameError}</div>}
      <input
      type="text"
      className="form_input"
      value={base}
      onChange={(e) => setBase(e.target.value)}
      placeholder="Enter base"
      ></input>
      {baseError && <div className="error_adddriver">{baseError}</div>}
      <input
      type="text"
      className="form_input"
      value={principalchief}
      onChange={(e) => setPrincipalchief(e.target.value)}
      placeholder="Enter team chief principal"
      ></input>
      {principalchiefError && <div className="error_adddriver">{principalchiefError}</div>}
      <input
      type="text"
      className="form_input"
      value={technicalchief}
      onChange={(e) => setTechnicalchief(e.target.value)}
      placeholder="Enter technical chief"
      ></input>
      {technicalchiefError && <div className="error_adddriver">{technicalchiefError}</div>}
      <input
      type="text"
      className="form_input"
      value={chassis}
      onChange={(e) => setChassis(e.target.value)}
      placeholder="Enter chassis"
      ></input>
      {chassisError && <div className="error_adddriver">{chassisError}</div>}
      <input
      type="text"
      className="form_input"
      value={powerunit}
      onChange={(e) => setPowerunit(e.target.value)}
      placeholder="Enter power unit"
      ></input>
      {powerunitError && <div className="error_adddriver">{powerunitError}</div>}
      <input
      type="number"
      className="form_input"
      value={firstentry}
      onChange={(e) => setFirstEntry(e.target.value)}
      placeholder="Enter first team entry"
      ></input>
      {firstentryError && <div className="error_adddriver">{firstentryError}</div>}
      <input
      type="text"
      className="form_input"
      value={wcc}
      onChange={(e) => setWcc(e.target.value)}
      placeholder="Enter world championships"
      ></input>
      {wccError && <div className="error_adddriver">{wccError}</div>}
      <input
      type="text"
      className="form_input"
      value={highestfinish}
      onChange={(e) => setHighestfinish(e.target.value)}
      placeholder="Enter highest race finish"
      ></input>
      {highestfinishError && <div className="error_adddriver">{highestfinishError}</div>}
      <input
      type="text"
      className="form_input"
      value={poleposition}
      onChange={(e) => setPoleposition(e.target.value)}
      placeholder="Enter pole positions"
      ></input>
      {polepositionError && <div className="error_adddriver">{polepositionError}</div>}
      <input
      type="text"
      className="form_input"
      value={fastestlap}
      onChange={(e) => setFastestlap(e.target.value)}
      placeholder="Enter fastest laps"
      ></input>
      {fastestlapError && <div className="error_adddriver">{fastestlapError}</div>}
      <input
      type="file"
      className="form_input"
      value={""}
      onChange={(e) => setTeamImageUpload(e.target.files[0])}
      placeholder="Enter img url"
      ></input>
      {teamPictureuploadError && <div className="error_adddriver">{teamPictureuploadError}</div>}
      <button onClick={handleSubmit} className="form_button" type="submit">Add team</button>
    </form>
  </div>
  );
}