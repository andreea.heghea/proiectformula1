import React, { useState } from "react";
import { ref, push } from "firebase/database";
import { db } from "../firebase/firebase";
import { uploadBytes } from "firebase/storage";
import { storage } from "../firebase/firebase";
import { ref as sRef } from "firebase/storage";
import { getDownloadURL } from "firebase/storage";
import { v4 } from "uuid";
import "../css/AddRaces.css";

export default function AddRaces() {
  const [gpname, setGpname] = useState("");
  const [gpyear, setGpyear] = useState("");
  const [circuitName, setCircuitName] = useState("");
  const [nolaps, setNolaps] = useState("");
  const [circuitlength, setCircuitlength] = useState("");
  const [racedistance, setRacedistance] = useState("");
  const [laprecord, setLaprecord] = useState("");
  const [laprecordowner, setLaprecordowner] = useState("");
  const [laprecordyear, setLaprecordyear] = useState("");
  const [racedate, setRacedate] = useState("");
  const [racetime, setRacetime] = useState("");
  const [pictureupload, setPictureupload] = useState("");
  const [uid, setUid]= useState("");
  const [imageUpload, setImageUpload] = useState(null);
  const [imageUrls, setImageUrls] = useState([]);
  const [gpnameError, setGpnameError] = useState(false);
  const [gpyearError, setGpyearError] = useState(false);
  const [circuitNameError, setCircuitNameError] = useState(false);
  const [nolapsError, setNolapsError] = useState(false);
  const [circuitlengthError, setCircuitlengthError] = useState(false);
  const [racedistanceError, setRacedistanceError] = useState(false);
  const [laprecordError, setLaprecordError] = useState(false);
  const [laprecordyearError, setLaprecordyearError] = useState(false);
  const [laprecordownerError, setLaprecordownerError] = useState(false);
  const [racedateError, setRacedateError] = useState(false);
  const [racetimeError, setRacetimeError] = useState(false);
  const [pictureuploadError, setPictureuploadError] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    const uid=v4();
    const newRace = {
      gpname : gpname,
      gpyear : gpyear,
      circuitName : circuitName,
      nolaps : nolaps,
      circuitlength : circuitlength,
      racedistance : racedistance,
      laprecord : laprecord,
      laprecordowner : laprecordowner,
      laprecordyear : laprecordyear,
      racedate : racedate,
      racetime : racetime,
      pictureupload : pictureupload,
      uid : uid
    };

    const raceRef = ref(db, "race");
    push(raceRef, newRace)
    .then(() => {
      console.log("Race added successfully!");
      setGpname("");
      setGpyear("");
      setCircuitName("");
      setNolaps("");
      setCircuitlength("");
      setRacedistance("");
      setLaprecord("");
      setLaprecordowner("");
      setLaprecordyear("");
      setRacedate("");
      setRacetime("");
      setUid("");
      setPictureupload("");
    })
    .catch((error) => {
      if(!gpname) {
        setGpnameError("Grand Prix name cannot be null");
      } 
      else setGpnameError("");
      if(!gpyear) {
        setGpyearError("Grand Prix year cannot be null");
      } 
      else setGpyearError("");
      if(gpyear < 1950) {
        setGpyearError("Grand prix year must be greater than 1950")
      }
      if(!circuitName) {
        setCircuitNameError("Circuir name cannot be null");
      } 
      else setCircuitNameError(""); 
      if(!nolaps) {
        setNolapsError("Lap number cannot be null");
      } 
      else setNolapsError("");
      if(nolaps > 78) {
        setNolapsError("Number of laps cannot exceed 78");
      }
      if(!circuitlength) {
        setCircuitlengthError("Circuit length cannot be null");
      } 
      else setCircuitlengthError("");
      if (circuitlength > 7.004) {
        setCircuitlengthError("Circuit length cannot exceed 7.004");
      }
      if(!racedistance) {
        setRacedistanceError("Race distance cannot be null");
      } 
      else setRacedistanceError("");
      if(racedistance > 310) {
        setRacedistanceError("Race distance cannot exceed 310");
      }
      if(!laprecord) {
        setLaprecordError("Lap recordcannot be null");
      } 
      else setLaprecordError(""); 
      if(!laprecordowner) {
        setGpnameError("Lap record owner cannot be null");
      } 
      else setGpnameError(""); 
      if(!laprecordyear) {
        setLaprecordyearError("Lap record year cannot be null");
      } 
      else setLaprecordyearError("");
      if(laprecordyear < 1950){
        setLaprecordyearError("Lap record year must be greater than 1950")
      }
      if(!racedate) {
        setRacedateError("Race date cannot be null");
      } 
      else setRacedateError(""); 
      if(!racetime) {
        setRacetimeError("Race time cannot be null");
      } 
      else setRacetimeError(""); 
      if(!pictureupload) {
        setPictureuploadError("Picture uploaad field cannot be null");
      } 
      else setPictureuploadError(""); 
    });

    if (imageUpload == null) return;
    const imageRef = sRef(storage, `images/circuit/${uid}`);
    uploadBytes(imageRef, imageUpload).then((snapshot) => {
      getDownloadURL(snapshot.ref).then((url) => {
        setImageUrls((prev) => [...prev, url]);
      });
    });
  }

  return(
  <div className="add_races">
    <form onSubmit={handleSubmit}>
      <h1>Add Race</h1>
      <input
      type="text"
      className="form_input"
      value={gpname}
      onChange={(e) => setGpname(e.target.value)}
      placeholder="Enter Grand Prix name"
      ></input>
      {gpnameError && <div className="error_adddriver">{gpnameError}</div>}
      <input
      type="text"
      className="form_input"
      value={gpyear}
      onChange={(e) => setGpyear(e.target.value)}
      placeholder="Enter first Grand Prix year"
      ></input>
      {gpyearError && <div className="error_adddriver">{gpyearError}</div>}
      <input
      type="text"
      className="form_input"
      value={circuitName}
      onChange={(e) => setCircuitName(e.target.value)}
      placeholder="Enter circuit name"
      ></input>
      {circuitNameError && <div className="error_adddriver">{circuitNameError}</div>}
      <input
      type="text"
      className="form_input"
      value={nolaps}
      onChange={(e) => setNolaps(e.target.value)}
      placeholder="Enter number of laps"
      ></input>
      {nolapsError && <div className="error_adddriver">{nolapsError}</div>}
      <input
      type="text"
      className="form_input"
      value={circuitlength}
      onChange={(e) => setCircuitlength(e.target.value)}
      placeholder="Enter circuit length"
      ></input>
      {circuitlengthError && <div className="error_adddriver">{circuitlengthError}</div>}
      <input
      type="text"
      className="form_input"
      value={racedistance}
      onChange={(e) => setRacedistance(e.target.value)}
      placeholder="Enter race distance"
      ></input>
      {racedistanceError && <div className="error_adddriver">{racedistanceError}</div>}
      <input
      type="text"
      className="form_input"
      value={laprecord}
      onChange={(e) => setLaprecord(e.target.value)}
      placeholder="Enter lap record"
      ></input>
      {laprecordError && <div className="error_adddriver">{laprecordError}</div>}
      <input
      type="text"
      className="form_input"
      value={laprecordowner}
      onChange={(e) => setLaprecordowner(e.target.value)}
      placeholder="Enter lap record owner"
      ></input>
      {laprecordownerError && <div className="error_adddriver">{laprecordowner}</div>}
      <input
      type="number"
      className="form_input"
      value={laprecordyear}
      onChange={(e) => setLaprecordyear(e.target.value)}
      placeholder="Enter lap record year"
      ></input>
      {laprecordyearError && <div className="error_adddriver">{laprecordyearError}</div>}
      <input
      type="date"
      className="form_input"
      value={racedate}
      onChange={(e) => setRacedate(e.target.value)}
      placeholder="Enter the upcoming date of the race"
      ></input>
      {racedateError && <div className="error_adddriver">{racedateError}</div>}
      <input
      type="time"
      className="form_input"
      value={racetime}
      onChange={(e) => setRacetime(e.target.value)}
      placeholder="Enter the time of the race"
      ></input>
      {racetimeError && <div className="error_adddriver">{racetimeError}</div>}
      <input
      type="file"
      className="form_input"
      value={""}
      onChange={(e) => setImageUpload(e.target.files[0])}
      placeholder="Enter img url"
      ></input>
      {pictureuploadError && <div className="error_adddriver">{pictureuploadError}</div>}
      <button className="form_button" type="submit">Add race</button>
    </form>
  </div>
  );
}