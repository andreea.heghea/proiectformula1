import React, { useState } from "react";
import { ref, push } from "firebase/database";
import { db } from "../firebase/firebase";
import { uploadBytes } from "firebase/storage";
import { storage } from "../firebase/firebase";
import { ref as sRef } from "firebase/storage";
import { getDownloadURL } from "firebase/storage";
import { v4 } from "uuid";
import "../css/AddDrivers.css";


export default function AddDrivers() {
  const [drivername, setDrivername] = useState("");
  const [team, setTeam] = useState("");
  const [racingnumber, setRacingNumber] = useState("");
  const [country, setCountry] = useState("");
  const [podiums, setPodiums] = useState("");
  const [points, setPoints] = useState("");
  const [gpenter, setGpenter] = useState("");
  const [wc, setWc] = useState("");
  const [highestracefinish, setHighestracefinish] = useState("");
  const [highestgridposition, setHighestgridposition] = useState("");
  const [datebirth, setDatebirth] = useState("");
  const [placebirth, setPlacebirth] = useState("");
  const [pictureupload, setPictureupload] = useState("");
  const [uid, setUid]= useState("");
  const [imageUpload, setImageUpload] = useState(null);
  const [imageUrls, setImageUrls] = useState([]);
  const [drivernameError, setDrivernameError] = useState(false);
  const [teamError, setTeamError] = useState(false);
  const [racingNumberError, setRacingNumberError] = useState(false);
  const [countryError, setCountryError] = useState(false);
  const [podiumsError, setPodiumsError] = useState(false);
  const [pointsError, setPointsError] = useState(false);
  const [gpenterError, setGpenterError] = useState(false);
  const [wcError, setWcError] = useState(false);
  const [highestracefinishError, setHighestracefinishError] = useState(false);
  const [highestgridpositionError, setHighestgridpositionError] = useState(false);
  const [datebirthError, setDatebirthError] = useState(false);
  const [placebirthError, setPlacebirthError] = useState(false);
  const [pictureuploadError, setPictureuploadError] = useState(false);

  const handleSubmit = (e) => {
    const uid=v4();
    e.preventDefault();
    const newDriver = {
      drivername : drivername,
      team : team,
      racingnumber : racingnumber,
      country : country,
      podiums : podiums,
      points : points,
      gpenter : gpenter,
      wc : wc,
      highestracefinish : highestracefinish,
      highestgridposition : highestgridposition,
      datebirth : datebirth,
      placebirth : placebirth,
      pictureupload : pictureupload,
      uid : uid
    };

    const driveRef = ref(db, "driver");
    push(driveRef, newDriver)
    .then(() => {
      setDrivername("");
      setTeam("");
      setCountry("");
      setPodiums("");
      setPoints("");
      setGpenter("");
      setWc("");
      setHighestracefinish("");
      setHighestgridposition("");
      setDatebirth("");
      setPlacebirth("");
      setRacingNumber("");
      setPictureupload("");
      setUid("");
    })
    .catch((error) => {
      if(!drivername) {
        setDrivernameError("Driver name cannot be null");
      } 
      else setDrivernameError(""); 
      if(!team) {
        setTeamError("Team cannot be null");
      }
      else setTeamError("");
      if(!racingnumber) {
        setRacingNumberError("Racing number cannot be null");
      }
      else setRacingNumberError("");
      if(racingnumber < 0 || racingnumber > 100){
        setRacingNumberError("Racing number must be between 1 and 99");
      }    
      if(!country) {       
        setCountryError("Country cannot be null");
      }
      else setCountryError("");
      if(!podiums) {        
        setPodiumsError("Podium number cannot be null");
      }
      else setPodiumsError("");
      if(podiums < 0) {
        setPodiumsError("Podiums must be higher than 0")
      } 
      if(!points) {        
        setPointsError("Points cannot be null");
      }
      else setPointsError("");
      if(points < 0) {
        setPointsError("Points must be positive");
      }
      if(!gpenter) {        
        setGpenterError("Grand Prix enter cannot be null");
      }
      else setGpenterError("");
      if(gpenter < 0) {
        setGpenterError("Grand Prix enter number must be positive");
      }
      if(!wc) {  
        setWcError("World Championships cannot be null");
      }
      else setWcError("");
      if(wc < 0) {
        setWcError("World championships number must be positive");
      }
      if(!highestracefinish) { 
        setHighestracefinishError("Highest race finish cannot be null");
      }
      else setHighestracefinishError("");
      if(highestracefinish < 0 || highestracefinish > 20) {
        setHighestracefinishError("Highest race finish must be greater than 0 but less than 20");
      }
      if(!highestgridposition) {      
        setHighestgridpositionError("Highest grid position cannot be null");
      }
      else 
      setHighestgridpositionError("");
      if(highestgridposition < 0 || highestgridposition > 20) {
        setHighestgridpositionError("Highest grid position must be greater than 0 but less than 20");
      }
      if(!datebirth) {        
        setDatebirthError("Date of birth cannot be null");
      }
      else setDatebirthError("");
      if(!placebirth) {     
        setPlacebirthError("Place of birth cannot be null");
      }
      else setPlacebirthError("");
      if(!imageUpload) {
        setPictureuploadError("Picture upload cannot be null");
      }
      else setPictureuploadError("");
    });

    if (imageUpload == null) return;
    const imageRef = sRef(storage, `images/driver/${uid}`);
    uploadBytes(imageRef, imageUpload).then((snapshot) => {
      getDownloadURL(snapshot.ref).then((url) => {
        setImageUrls((prev) => [...prev, url]);
      });
    });
  }
  
  return(
  <div className="add_drivers">
      <h1>Add Driver</h1>
      <input
      type="text"
      id="text-field-with-error"
      className="form_input"
      value={drivername}
      onChange={(e) => setDrivername(e.target.value)}
      placeholder="Enter driver name"
      ></input>
      {drivernameError && <div className="error_adddriver">{drivernameError}</div>}
      <input
      type="text"
      className="form_input"
      value={team}
      onChange={(e) => setTeam(e.target.value)}
      placeholder="Enter team"
      ></input>
      {teamError && <div className="error_adddriver">{teamError}</div>}
      <input
      type="number"
      className="form_input"
      value={racingnumber}
      onChange={(e) => setRacingNumber(e.target.value)}
      placeholder="Enter racing number"
      ></input>
      {racingNumberError && <div className="error_adddriver">{racingNumberError}</div>}
      <input
      type="text"
      className="form_input"
      value={country}
      onChange={(e) => setCountry(e.target.value)}
      placeholder="Enter country"
      ></input>
      {countryError && <div className="error_adddriver">{countryError}</div>}
      <input
      type="text"
      className="form_input"
      value={podiums}
      onChange={(e) => setPodiums(e.target.value)}
      placeholder="Enter podiums"
      ></input>
      {podiumsError && <div className="error_adddriver">{podiumsError}</div>}
      <input
      type="number"
      className="form_input"
      value={points}
      onChange={(e) => setPoints(e.target.value)}
      placeholder="Enter points"
      ></input>
      {pointsError && <div className="error_adddriver">{pointsError}</div>}
      <input
      type="number"
      className="form_input"
      value={gpenter}
      onChange={(e) => setGpenter(e.target.value)}
      placeholder="Enter Grand Prix entered"
      ></input>
      {gpenterError && <div className="error_adddriver">{gpenterError}</div>}
      <input
      type="text"
      className="form_input"
      value={wc}
      onChange={(e) => setWc(e.target.value)}
      placeholder="Enter World Championships"
      ></input>
      {wcError && <div className="error_adddriver">{wcError}</div>}
      <input
      type="text"
      className="form_input"
      value={highestracefinish}
      onChange={(e) => setHighestracefinish(e.target.value)}
      placeholder="Enter highest race finish"
      ></input>
      {highestracefinishError && <div className="error_adddriver">{highestracefinishError}</div>}
      <input
      type="number"
      className="form_input"
      value={highestgridposition}
      onChange={(e) => setHighestgridposition(e.target.value)}
      placeholder="Enter highest grid position"
      ></input>
      {highestgridpositionError && <div className="error_adddriver">{highestgridpositionError}</div>}
      <input
      type="date"
      name="date"
      className="form_input"
      value={datebirth}
      onChange={(e) => setDatebirth(e.target.value)}
      placeholder="Enter date of birth"
      ></input>
      {datebirthError && <div className="error_adddriver">{datebirthError}</div>}
      <input
      type="text"
      className="form_input"
      value={placebirth}
      onChange={(e) => setPlacebirth(e.target.value)}
      placeholder="Enter place of birth"
      ></input>
      {placebirthError && <div className="error_adddriver">{placebirthError}</div>}
      <input
      type="file"
      className="form_input"
      value={""}
      onChange={(e) => setImageUpload(e.target.files[0])}
      placeholder="Enter img url"
      ></input>
      {pictureuploadError && <div className="error_adddriver">{pictureuploadError}</div>}
      <button onClick={handleSubmit} className="form_button" type="submit">Add driver</button>
  </div>
  );
}