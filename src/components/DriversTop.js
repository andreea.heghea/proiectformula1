import React, { useState, useEffect } from "react";
import { getDatabase, ref, onValue } from "firebase/database";
import max from "../img/max.png";
import sergio from "../img/sergio.png";
import fernando from "../img/fernando.png";
import "../css/DriversTop.css";

function DriversTop() {
  const [driverStandings, setConstructors] = useState([]);
  const sortedDriverStandings = [...driverStandings].sort((a, b) => b.driverPoints - a.driverPoints);

  useEffect(() => {
    
    const db = getDatabase();
    const driverStandingsRef = ref(db, "driverStandings");

    const unsubscribe = onValue(driverStandingsRef, (snapshot) => {
      const driverStandingsData = snapshot.val();
      if (driverStandingsData) {
        const driverStandingsArray = Object.values(driverStandingsData);
        setConstructors(driverStandingsArray);
      }
    });

    return () => {
      unsubscribe();
    };
  }, []);

  return (
    <div className="driversTop_page">
     <p>Driver Standings</p>
  <div className="table_wrap">
  <table className="driversTop_table">
    <thead>
      <tr>
        <th>Position</th>
        <th>Name</th>
        <th>Points</th>
      </tr>
    </thead>
    <tbody>
      {sortedDriverStandings.slice(0, 3).map((driverStandings, index) => (
        <tr key={driverStandings.driverStandingsName} className={index % 2 === 0 ? "even_row_drivers" : "odd_row_drivers"}>
          <td>{index + 1}</td>
          <td>
            {driverStandings.driverStandingsName}
            </td>
          <td>{driverStandings.driverPoints}</td>
        </tr>
      ))}
    </tbody>
  </table>
  </div>
  <div className="img">
    <img className="sergio" src={sergio} alt="Sergio" />
    <img className="max" src={max} alt="F1 Logo" />
    <img className="fernando" src={fernando} alt="Alonso" />
  </div>
</div>

);
}

export default DriversTop;