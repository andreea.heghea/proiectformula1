import React, {useState, useEffect} from "react";
import { onValue} from "firebase/database";
import { ref } from "firebase/database";
import "../css/ModalTeam.css";
import { db } from "../firebase/firebase";

function ModalTeam ({closeModalTeam, selectedTeam}) {
  const [teams, setTeams] = useState([]);

  useEffect(() => {
    const countRef = ref(db, "team/");
    onValue(countRef, (snapshot) => {
      const data = snapshot.val();
      if (data) {
        const newPosts = Object.keys(data).map((key) => ({
          id: key,
          ...data[key],
        }));
        setTeams(newPosts);
      } else {
        setTeams([]);
      }
    });
    
  }, []);

  return (
    <div className="modal_background">
      <div className="modal_container">
        <div className="title_closebutton">
          <button onClick={() => closeModalTeam(false)}> X </button>
        </div>
          {selectedTeam && (
          <div>
            <li key={selectedTeam.teamname}>
            <div className="team_name">{selectedTeam.teamname}</div>
            <div className="team_info_container">
              <div className="team_info_modal">
                <div className="label">Base</div>
                <div className="value">{selectedTeam.base}</div>
              </div>
              <div className="team_info_modal">
                <div className="label">Team Principal</div>
                <div className="value">{selectedTeam.principalchief}</div>
              </div>
              <div className="team_info_modal">
                <div className="label">Team Technical Chief</div>
                <div className="value">{selectedTeam.technicalchief}</div>
              </div>
              <div className="team_info_modal">
                <div className="label">Chassis</div>
                <div className="value">{selectedTeam.chassis}</div>
              </div>
              <div className="team_info_modal">
                <div className="label">Power unit</div>
                <div className="value">{selectedTeam.powerunit}</div>
              </div>
              <div className="team_info_modal">
                <div className="label">First Team Entry</div>
                <div className="value">{selectedTeam.firstentry}</div>
              </div>
              <div className="team_info_modal">
                <div className="label">World Constructor Championships</div>
                <div className="value">{selectedTeam.wcc}</div>
              </div>
              <div className="team_info_modal">
                <div className="label">Highest race finish</div>
                <div className="value">{selectedTeam.highestfinish}</div>
              </div>
              <div className="team_info_modal">
                <div className="label">Pole positions</div>
                <div className="value">{selectedTeam.poleposition}</div>
              </div>
              <div className="team_info_modal">
                <div className="label">Fastest laps</div>
                <div className="value">{selectedTeam.fastestlap}</div>
              </div>
              <div className="team_photo_container">
                <img className="team_photo" src={selectedTeam.teamImageUrls} />
              </div>
            </div>      
          </li>
      </div>
        )}
      </div>
    </div>

  );

}

export default ModalTeam;