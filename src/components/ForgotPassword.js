import React, { useState } from "react";
import "../css/ForgotPassword.css";
import { auth } from "../firebase/firebase";
import { sendPasswordResetEmail } from "firebase/auth";
import { Link } from "react-router-dom";

export default function ForgotPassword() {
  const [email, setEmail] = useState("");
  const [error, setError] = useState(false);
  const [status, setStatus] = useState(undefined);

    const forgotPassword = (e) => {
      e.preventDefault();
      sendPasswordResetEmail(auth, email)
      .then(() => {
        setStatus({type: "success"});
      })
      .catch((error) => {
        if (error.code === "auth/user-not-found") {
          setError("User not found. Please check your email.");
        }
        else if (error.code === "auth/missing-email") {
          setError("Email is missing!");
        } else {
          setError("An error occurred during this action!");
        }
      });

      setTimeout(() => {
        setError(null);
      }, 2000); 
}
  

  return(
    <div className="forgotpassword">
      <form onSubmit={forgotPassword}>
        <h1>Forgot Password</h1>
        <input
          type="email" 
          className="form_forgot"
          placeholder="Enter your email" 
          value={email}
          onChange={(e) => setEmail(e.target.value)}
         ></input>
          {status?.type === "success" && <label className="label_success">Email has been sent successfully!</label>}
        <button className="form_button" type="submit">Submit</button>
        <p class="form_text"> 
        <Link to="/signin" class="form_link">Sign in</Link>
        {error && <p style={{ color: "red" }}>{error}</p>}
      </p>
      </form>
    </div>
    );
}