import React, { useEffect, useState }  from "react";
import "../css/Navbar.css";
import logo from "../img/f1_logo.png";
import { auth } from "../firebase/firebase";
import { onAuthStateChanged, signOut } from "firebase/auth";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";

export default function Navbar() {
  const navigate = useNavigate();
  const [authUser, setAuthUser] = useState({ isAdmin: false });
  
  useEffect(() => {
    const listen = onAuthStateChanged(auth, (user) => {
        if(user) {
          const isAdmin = user.email.endsWith("@admin.com");

          setAuthUser({ ...user, isAdmin });
        } else {
          setAuthUser(null);
        }
    });

    return () => {
      listen();
    };
  }, []);

  const userSignOut = () => {
    signOut(auth).then(() => {
      console.log("sign out successful");
    }).catch(error => console.log(error));
    navigate("/signin");
  }

  return <nav className="nav">
    <img src={logo} alt="F1 Logo" />
    <ul>
      <li>
        <li><Link to="/home">HOME</Link></li>
        <li><Link to="/raceresults">RESULTS</Link>
        <div className="results-dropdown">
            <ul className="results-list">
              <li className="results_li"><Link to="/driverstandings">Driver Standings</Link></li>
              <li className="results_li"><Link to="/constructorstandings">Constructor Standings</Link></li>
            </ul>
        </div>
        </li>
        <li><Link to="/drivers">DRIVERS</Link></li>
        <li><Link to="/teams">TEAMS</Link></li>
        <li><Link to="/circuits">CIRCUITS</Link></li>
        {authUser.isAdmin && (
        <li><Link>ADD</Link>
        <div className="add-dropdown">
          <ul className="add_list">
            <li><Link to="/adddrivers">ADD DRIVERS</Link></li>
            <li><Link to="/addteams">ADD TEAMS</Link></li>
            <li><Link to="/addraces">ADD RACES</Link></li>
            <li><Link to="/addconstructorstandings">ADD CONSTRUCTOR</Link></li>
            <li><Link to="/addraceresults">ADD RACE RESULTS</Link></li>
            <li><Link to="/adddriverstandings">ADD DRIVER STANDINGS</Link></li>
          </ul>
        </div>
        </li>
        )}
        <li><Link to="/survey">SURVEY</Link></li>
        <li><Link to="/quiz">QUIZ</Link></li>
        <li className="username">{authUser && <p>{`Logged in as ${authUser.email}`}</p>}</li>
      </li>
      <div className="SigninButton">
        <Link to="/signin"><button onClick={userSignOut} className="btn">SIGN OUT</button></Link>
    </div>
    </ul>
  </nav>
}
