import React, { useState } from "react";
import { ref, push } from "firebase/database";
import { db } from "../firebase/firebase";
import "../css/AddConstructors.css";

export default function AddDrivers() {
  const [constructorTeamName, setconstructorTeamName] = useState("");
  const [constructorPoints, setConstructorPoints] = useState("");
  const [constructorTeamNameError, setconstructorTeamNameError] = useState(false);
  const [constructorPointsError, setConstructorPointsError] = useState(false);


  const handleSubmit = (e) => {
    e.preventDefault();
    const newConstructor = {
      constructorTeamName : constructorTeamName,
      constructorPoints : constructorPoints
    };

    const constructorRef = ref(db, "constructor");
    push(constructorRef, newConstructor)
    .then(() => {
      console.log("Constructor added successfully!");
      setconstructorTeamName("");
      setConstructorPoints("");
    })
    .catch((error) => {
      if(!constructorTeamName) {
        setconstructorTeamNameError("Constructor team name cannot be null");
      } 
      else setconstructorTeamNameError("");
      if(!constructorPoints) {
        setConstructorPointsError("Constructor points cannot be null");
      } 
      else setConstructorPointsError("");
      if(constructorPoints < 0) {
        setConstructorPointsError("Constructor points must be positive");
      }
    });  
  }
 
  return(
  <div className="add_constructors">
    <form onSubmit={handleSubmit}> 
      <h1>Add Constructor</h1>
      <input
      type="text"
      className="form_input"
      value={constructorTeamName}
      onChange={(e) => setconstructorTeamName(e.target.value)}
      placeholder="Enter constructor name"
      ></input>
      {constructorTeamNameError && <div className="error_adddriver">{constructorTeamNameError}</div>}
      <input
      type="number"
      className="form_input"
      value={constructorPoints}
      onChange={(e) => setConstructorPoints(e.target.value)}
      placeholder="Enter constructor points"
      ></input>
      {constructorPointsError && <div className="error_adddriver">{constructorPointsError}</div>}
      <button className="form_button" type="submit">Add constructor</button>
    </form>
  </div>
  );
}