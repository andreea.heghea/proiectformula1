import React from 'react';
import logo from "../img/f1_logo.png";

function Footer() {
  return (
    <footer>
    <p className="p_text"><img  className="footer_logo" src={logo} alt="F1 Logo" />&copy;</p>
    <p>All rights reserved.</p>
  </footer>
   
  );
}

export default Footer;
