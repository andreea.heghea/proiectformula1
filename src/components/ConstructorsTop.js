import React, { useState, useEffect } from "react";
import { getDatabase, ref, onValue } from "firebase/database";
import "../css/ConstructorsTop.css";

function ConstructorsTop() {
  const [constructors, setConstructors] = useState([]);
  const sortedConstructors = [...constructors].sort((a, b) => b.constructorPoints - a.constructorPoints);

  useEffect(() => {
    const db = getDatabase();
    const constructorRef = ref(db, "constructor");

    const unsubscribe = onValue(constructorRef, (snapshot) => {
      const constructorsData = snapshot.val();
      if (constructorsData) {
        const constructorsArray = Object.values(constructorsData);
        setConstructors(constructorsArray);
      }
    });

    return () => {
      unsubscribe();
    };
  }, []);
  
  return (
 <div className="constructorstop_page">
    <p>Constructor Standings</p>
      <div className="table_wrapper">
  <table className="constructors_table">
    <thead>
      <tr>
        <th>Position</th>
        <th>Team</th>
        <th>Points</th>
      </tr>
    </thead>
    <tbody>
      {sortedConstructors.slice(0, 3).map((constructor, index) => (
        <tr key={constructor.constructorTeamName} className={index % 2 === 0 ? "even_row_constructors" : "odd_row_constructors"}>
          <td>{index + 1}</td>
          <td>{constructor.constructorTeamName}</td>
          <td>{constructor.constructorPoints}</td>
        </tr>
      ))}
    </tbody>
  </table>
  </div>
</div> 
);
}

export default ConstructorsTop;
