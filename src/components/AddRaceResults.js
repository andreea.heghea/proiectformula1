import React, { useState } from "react";
import { ref, push } from "firebase/database";
import { db } from "../firebase/firebase";
import "../css/AddRaceResults.css";

export default function AddRaceResults() {
  const [grandPrix, setGrandPrix] = useState("");
  const [grandPrixDate, setGrandPrixDate] = useState("");
  const [winner, setWinner] = useState("");
  const [winnerConstructor, setWinnerConstructor] = useState("");
  const [lapNumber, setLapnumber]= useState("");
  const [fastestLapOwner, setFastestLapOwner] = useState("");
  const [grandPrixError, setGrandPrixError] = useState(false);
  const [grandPrixDateError, setGrandPrixDateError] = useState(false);
  const [winnerError, setWinnerError] = useState(false);
  const [winnerConstructorError, setWinnerConstructorError] = useState(false);
  const [lapNumberError, setLapnumberError] = useState(false);
  const [fastestLapOwnerError, setFastestLapOwnerError] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    const newRaceResult = {
      grandPrix : grandPrix,
      grandPrixDate : grandPrixDate,
      winner : winner,
      winnerConstructor : winnerConstructor,
      lapNumber : lapNumber,
      fastestLapOwner : fastestLapOwner
    };

    const raceResultsRef = ref(db, "raceResults");
    push(raceResultsRef, newRaceResult)
    .then(() => {
      console.log("Race result added successfully!");
      setGrandPrix("");
      setGrandPrixDate("");
      setWinner("");
      setWinnerConstructor("");
      setLapnumber("");
      setFastestLapOwner("");
    })
    .catch((error) => {
      if(!grandPrix) {
        setGrandPrixError("Grand Prix name cannot be null");
      } 
      else setGrandPrixError("");
      if(!grandPrixDate) {
        setGrandPrixDateError("Grand Prix date cannot be null");
      } 
      else setGrandPrixDateError("");
      if(!winner) {
        setWinnerError("Winner name cannot be null");
      } 
      else setWinnerError("");
      if(!winnerConstructor) {
        setWinnerConstructorError("Winner constructor name cannot be null");
      } 
      else setWinnerConstructorError("");
      if(!lapNumber) {
        setLapnumberError("Lap number cannot be null");
      } 
      else setLapnumberError("");
      if(lapNumber > 78){
        setLapnumberError("Lap number cannot exceed 78")
      }
      if(!fastestLapOwner) {
        setFastestLapOwnerError("Fastest lap owner cannot be null");
      } 
      else setFastestLapOwnerError("");
    });  
  }
 
  return(
  <div className="add_raceresult">
    <form onSubmit={handleSubmit}> 
      <h1>Add Race result</h1>
      <input
      type="text"
      className="form_input"
      value={grandPrix}
      onChange={(e) => setGrandPrix(e.target.value)}
      placeholder="Enter Grand Prix name"
      ></input>
      {grandPrixError && <div className="error_adddriver">{grandPrixError}</div>}
      <input
      type="date"
      className="form_input"
      value={grandPrixDate}
      onChange={(e) => setGrandPrixDate(e.target.value)}
      placeholder="Enter Grand Prix date"
      ></input>
      {grandPrixDateError && <div className="error_adddriver">{grandPrixDateError}</div>}
      <input
      type="text"
      className="form_input"
      value={winner}
      onChange={(e) => setWinner(e.target.value)}
      placeholder="Enter Grand Prix winner"
      ></input>
      {winnerError && <div className="error_adddriver">{winnerError}</div>}
      <input
      type="text"
      className="form_input"
      value={winnerConstructor}
      onChange={(e) => setWinnerConstructor(e.target.value)}
      placeholder="Enter Grand Prix Winning Constructor"
      ></input>
      {winnerConstructorError && <div className="error_adddriver">{winnerConstructorError}</div>}
      <input
      type="number"
      className="form_input"
      value={lapNumber}
      onChange={(e) => setLapnumber(e.target.value)}
      placeholder="Enter lap number"
      ></input>
      {lapNumberError && <div className="error_adddriver">{lapNumberError}</div>}
      <input
      type="text"
      className="form_input"
      value={fastestLapOwner}
      onChange={(e) => setFastestLapOwner(e.target.value)}
      placeholder="Enter fastest lap owner"
      ></input>
      {fastestLapOwnerError && <div className="error_adddriver">{fastestLapOwnerError}</div>}
      <button className="form_button" type="submit">Add race result</button>
    </form>
  </div>
  );
}