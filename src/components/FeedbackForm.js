import React, { useRef } from "react";
import emailjs from "@emailjs/browser";
import "../css/FeedbackForm.css";

function FeedbackForm() {
  const ContactUs = () => {
    const form = useRef();
  
    const sendEmail = (e) => {
      e.preventDefault();
  
      emailjs.sendForm('service_mc0zovk', 'service_mc0zovk', form.current, 'J0Cw2QpUH7zzHUHda')
        .then((result) => {
            console.log(result.text);
        }, (error) => {
            console.log(error.text);
        });
    };
    return (
      <div>
      <form ref={form} onSubmit={sendEmail}>
        <label>Name</label>
        <input type="text" name="user_name" />
        <label>Email</label>
        <input type="email" name="user_email" />
        <label>Message</label>
        <textarea name="message" />
        <input type="submit" value="Send" />
      </form>
      </div>
    );
  };
}

export default FeedbackForm;