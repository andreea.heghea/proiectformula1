import React, { useState } from "react";
import { ref, push } from "firebase/database";
import { db } from "../firebase/firebase";
import "../css/AddDriverStandings.css";

export default function AddDrivers() {
  const [driverStandingsName, setDriverStandingsName] = useState("");
  const [driverCountry, setDriverCountry] = useState("");
  const [driverPoints, setDriverPoints] = useState("");
  const [driverStandingsNameError, setDriverStandingsNameError] = useState(false);
  const [driverCountryError, setDriverCountryError] = useState(false);
  const [driverPointsError, setDriverPointsError] = useState(false);


  const handleSubmit = (e) => {
    e.preventDefault();
    const newDriverStanding = {
      driverStandingsName : driverStandingsName,
      driverCountry : driverCountry,
      driverPoints : driverPoints
    };

    const driverStandingsRef = ref(db, "driverStandings");
    push(driverStandingsRef, newDriverStanding)
    .then(() => {
      console.log("Driver for standings added successfully!");
      setDriverStandingsName("");
      setDriverCountry("");
      setDriverPoints("");
    })
    .catch((error) => {
      if(!driverStandingsName) {
        setDriverStandingsNameError("Driver name cannot be null");
      } 
      else setDriverStandingsNameError("");
      if(!driverCountry) {
        setDriverCountryError("Country cannot be null");
      } 
      else setDriverCountryError("");
      if(!driverPoints) {
        setDriverPointsError("Driver points cannot be null");
      } 
      else setDriverPointsError("");
      if(driverPoints < 0) {
        setDriverPointsError("Driver points must be positive");
      }
    });  
  }
 
  return(
  <div className="add_driverStandings">
    <form onSubmit={handleSubmit}> 
      <h1>Add Driver Standings</h1>
      <input
      type="text"
      className="form_input"
      value={driverStandingsName}
      onChange={(e) => setDriverStandingsName(e.target.value)}
      placeholder="Enter driver name"
      ></input>
       {driverStandingsNameError && <div className="error_adddriver">{driverStandingsNameError}</div>}
      <input
      type="text"
      className="form_input"
      value={driverCountry}
      onChange={(e) => setDriverCountry(e.target.value)}
      placeholder="Enter driver country"
      ></input>
       {driverCountryError && <div className="error_adddriver">{driverCountryError}</div>}
      <input
      type="number"
      className="form_input"
      value={driverPoints}
      onChange={(e) => setDriverPoints(e.target.value)}
      placeholder="Enter driver points"
      ></input>
       {driverPointsError && <div className="error_adddriver">{driverPointsError}</div>}
      <button className="form_button" type="submit">Add driver for standings</button>
    </form>
  </div>
  );
}