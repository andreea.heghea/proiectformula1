import React, { useState} from "react";
import { auth } from "../../firebase/firebase";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { useNavigate } from "react-router-dom";
import "./SignUp.css";
import { Link } from "react-router-dom";

const SignUp = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);
  const navigate = useNavigate();
  
  const signUp = async () => {
    let valid = true;
    if (email.includes("@admin.com")) {
      setError("Admin account is not permitted!");
      valid = false;
    }
    if (!email.includes("@") || !email.includes(".com")) {
      setError("Email must include @ and .com!");
      valid = false;
    }
    if (!valid) {
      return;
    } else {
    await createUserWithEmailAndPassword(auth, email, password)
    .then((useCredential) => {
      console.log(useCredential);
      navigate("/home");
    }).catch((error) => {
      if (error.code === "auth/missing-password") {
        setError("Password is missing!");
      }
      else if (error.code === "auth/invalid-email") {
        setError("Email is missing!");
      }
      else if (error.code === "auth/email-already-in-use") {
        setError("Email is already being used!");
      }else if (error.code === "auth/weak-password") {
        setError("Password must contain at least 6 characters!");
      }

      setTimeout(() => {
        setError(null);
      }, 2500); 
    });
    }
  }

  return(
  <div className="signup">
      <h1>Create Account</h1>
      <input
        type="email"
        className="form_input"
        placeholder="Enter your email" 
        value={email}
        onChange={(e) => setEmail(e.target.value)}
       ></input>
      <input 
        type="password"
        className="form_input"
        placeholder="Enter your password" 
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      ></input>
      <button onClick={signUp} className="form_button" type="submit">Sign up</button>
      <p className="form_text"> 
        <Link to="/signin" className="form_link">Already have an account? Sign in</Link>
        {error && <div className="error_signup">{error}</div>} 
      </p>
  </div>
  );
}

export default SignUp;