import React, { useState, useEffect} from "react";
import { auth } from "../../firebase/firebase";
import { signInWithEmailAndPassword } from "firebase/auth";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { onAuthStateChanged } from "firebase/auth";
import "./SignIn.css";

const SignIn = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);
  const [emailError, setEmailError] = useState(false);
  const [authUser, setAuthUser] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    const listen = onAuthStateChanged(auth, (user) => {
        if(user) {
          setAuthUser(user)
        } else {
          setAuthUser(null);
        }
    });
 
    return () => {
      listen();
    };
  }, []);

  const signIn = async () => {
    await signInWithEmailAndPassword(auth, email, password)
    .then((useCredential) => {
      console.log(useCredential);
      navigate("/home");
    }).catch((error) => {
      if (error.code === "auth/user-not-found") {
        setError("User not found!");
      } else if (error.code === "auth/wrong-password") {
        setError("Incorrect password!");
      }
      else if (error.code === "auth/missing-password") {
        setError("Password is missing!");
      }
      else if (error.code === "auth/invalid-email") {
        setError("Email is missing!");
      }
     
      setTimeout(() => {
        setError(null);
      }, 2000); 
    });
  }

  

  return(
  <div className="signin_form">
      <h1>Sign in</h1>
      <input
        type="email" 
        className="form_input_signin"
        placeholder="Enter your email" 
        value={email}
        onChange={(e) => setEmail(e.target.value)}
       ></input>
      <input 
        type="password"
        className="form_input_signin"
        placeholder="Enter your password" 
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      ></input>
      <button onClick={signIn} className="form_button" type="submit">Sign in</button>
      <p class="form_text">
        <Link to="/forgotpassword" class="form_link">Forgot your password?</Link> 
      </p>
      <p class="form_text"> 
        <Link to="/signup" class="form_link">Don't have an account? Create account</Link>
        {error && <div className="error_signin">{error}</div>}
        {emailError && <div className="error_signin">{emailError}</div>} 
      </p>
    
  </div>
  );
}

export default SignIn;