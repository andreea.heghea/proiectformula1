import React, { useState, useEffect } from "react";
import { ref, onValue } from "firebase/database";
import { listAll } from "firebase/storage";
import { storage } from "../firebase/firebase";
import { ref as sRef, getDownloadURL } from "firebase/storage";
import "../css/Drivers.css";
import ModalDriver from "../components/ModalDriver";
import { db } from "../firebase/firebase";

function Drivers() {
  const [drivers, setDrivers] = useState([]);
  const [openModalDriver, setOpenModalDriver] = useState(false);
  const [selectedDriver, setSelectedDriver] = useState(null);
  const [imageUrls, setImageUrls] = useState([]);
  const [imageNames, setImageNames] = useState([]);
  

  useEffect(() => {
    const countRef = ref(db, "driver/");
    onValue(countRef, (snapshot) => {
      const data = snapshot.val();
      if (data) {
        const newPosts = Object.keys(data).map((key) => ({
          id: key,
          ...data[key],
        }));
        setDrivers(newPosts);
      } else {
        setDrivers([]);
      }
    });

    const imagesRef = sRef(storage, `images/driver`);
    listAll(imagesRef)
    .then((res) => {
      const names = res.items.map((item) => item.name);
      setImageNames(names);
      return Promise.all(res.items.map((item) => getDownloadURL(item)));
    })
    .catch((error) => {
      console.log(error);
    })
    .then((urls) => {
      setImageUrls(urls);
    })
    .catch((error) => {
      console.log(error);
    });
    
  }, []);

  
  const matchImage = () => {
    drivers.map((item, index) => {
      imageUrls.map((url, index) => {
        if (imageNames[index].includes(item.uid)) {
          item.imageUrls = url;
        }
      });
    });
  };
  matchImage();



  const handleDriverClick = (driverName, image) => {
    const selectedDriverObject = drivers.find((driver) => driver.drivername === driverName);
    setSelectedDriver(selectedDriverObject);
  
    setOpenModalDriver(true);
  };

  return (
  <div className="drivers_page">
    <h2 className="header">Formula 1 Drivers List</h2>
    <div className="drivers_list" onClick={() =>{setOpenModalDriver(true);}}>
      {drivers.map((driver) => (
      <li key={driver.drivername}  onClick={() => handleDriverClick(driver.drivername, driver.imageUrls) }>
        <div>
          <div className="driver_info">
            <div>{driver.drivername}</div>
          </div>
          <ul>
            <div className="racing_number">
              <div className={`driver_number ${driver.drivername.toLowerCase()}`}>{driver.racingnumber}</div>
            </div>
            </ul>
            <div className="driver_info">
              <div className="label">Team</div>
              <div className="value">{driver.team}</div>
            </div>
            <div className="driver_info">
              <div className="label">Country</div>
              <div className="value">{driver.country}</div>
            </div>
            <div className="driver_info">
              <div className="label">Date of birth</div>
              <div className="value">{driver.datebirth}</div>
            </div>
            <div>
              <button className="openModalBtn" onClick={() => {setOpenModalDriver(true);}}>View more</button>
            </div>
          </div>
        </li> 
        ))}
      </div>
      {openModalDriver && <ModalDriver closeModalDriver={() => setOpenModalDriver(false)} selectedDriver={selectedDriver}/>}
  </div>
  );
}

export default Drivers;
