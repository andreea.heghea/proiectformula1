import React from "react";
import CountdownToNextRace from "./CountdownToNextRace";
import ImageSlideshow from "../components/ImageSlideshow";
import DriversTop from "../components/DriversTop";
import overtake from "../img/alonsovsham.PNG";
import redbull from "../img/rbcar.png";
import logo from "../img/f1_logo.png";
import ConstructorsTop from "../components/ConstructorsTop";
import "../css/Home.css";

export default function Home() {
  const slides = [
    {url: "http://localhost:3000/image-8.PNG", title: "Monaco"},
    {url: "http://localhost:3000/image-7.jpeg", title: "Podium"},
    {url: "http://localhost:3000/image-1.PNG", title: "Race"},
    {url: "http://localhost:3000/image-4.PNG", title: "Charles"},
    {url: "http://localhost:3000/image-3.jpg", title: "Lewis"},
    {url: "http://localhost:3000/image-2.jpg", title: "Overtake"},
    {url: "http://localhost:3000/image-9.PNG", title: "Sprint"},
    {url: "http://localhost:3000/image-5.PNG", title: "Crash"},
    {url: "http://localhost:3000/image-6.PNG", title: "Drivers"},
    {url: "http://localhost:3000/image-10.PNG", title: "Max"}
  ];
 
  return (
    <>
  <div>
      <p className="title">Race Highlights</p>
      <div className="container_style">
      <ImageSlideshow slides={slides}/>
      </div>
  </div>
  <div className="left_div">
  <CountdownToNextRace />
  </div>
  <div className="right_div">
  <DriversTop />
  </div>
  <div className="overtake_img">
    <p className="p">.</p>
    <p class="title-p"> Overtake of the season: ALONSO VS HAMILTON</p>
  <img className="overtake" src={overtake} alt="Overtake" />
  </div>
  <div>
    <ConstructorsTop />
    <img className="redbull" src={redbull} alt="Red Bull Car" />
  </div>
    <footer>
      <p className="p_text"><img  className="footer_logo" src={logo} alt="F1 Logo" />&copy;</p>
      <p>All rights reserved.</p>
    </footer>
  </>
  );
}