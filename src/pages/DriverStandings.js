import React, { useState, useEffect } from 'react';
import { getDatabase, ref, onValue, query, orderByChild, equalTo, get, set } from 'firebase/database';
import { db } from '../firebase/firebase';
import logo from "../img/f1_logo.png";
import '../css/DriverStandings.css';

function DriverStandings() {
  const [driverStandings, setConstructors] = useState([]);
  const sortedDriverStandings = [...driverStandings].sort((a, b) => b.driverPoints - a.driverPoints);

  useEffect(() => {
    
    const db = getDatabase();
    const driverStandingsRef = ref(db, "driverStandings");

    const unsubscribe = onValue(driverStandingsRef, (snapshot) => {
      const driverStandingsData = snapshot.val();
      if (driverStandingsData) {
        const driverStandingsArray = Object.values(driverStandingsData);
        setConstructors(driverStandingsArray);
      }
    });

    return () => {
      unsubscribe();
    };
  }, []);

    const extractLastName = (fullName) => {
      const name = fullName.split(" ");
      return name.length > 1 ? name[name.length - 1] : fullName;
    }

  const updateDriverStandingPoints = async (driverStandingsName, updatedPoints) => {
    const driverStandingsRef = ref(db, "driverStandings");
    const driverStandingsQuery = query(driverStandingsRef, orderByChild("driverStandingsName"), equalTo(driverStandingsName));
  
    try {
      const snapshot = await get(driverStandingsQuery);
      if (snapshot.exists()) {
        const driverStandingsKey = Object.keys(snapshot.val())[0];
        const driverStandingsRef = ref(db, `driverStandings/${driverStandingsKey}/driverPoints`);
  
        set(driverStandingsRef, updatedPoints)
          .then(() => {
            console.log(`${driverStandingsName} points updated successfully!`);
          })
          .catch((error) => {
            console.error(`Error updating points for ${driverStandingsName}:`, error);
          });
      } else {
        console.log(`Driver "${driverStandingsName}" not found in the database.`);
      }
    } catch (error) {
      console.error("Error getting drivers:", error);
    }
  };
  
  const driverNameToBeUpdated = "Esteban OCON";
  const updatedPointsValue = 35;
  updateDriverStandingPoints(driverNameToBeUpdated, updatedPointsValue);

  document.body.style = "background: white";

  return (
  <>
  <div className="driverStanding_page">
    <p>2023 Driver Standings</p>
    <div className="table_wrapper">
      <table className="driverStanding_table">
        <thead>
          <tr>
            <th>Position</th>
            <th>Name</th>
            <th>Country</th>
            <th>Points</th>
            </tr>
            </thead>
            <tbody>
              {sortedDriverStandings.map((driverStandings, index) => (
              <tr key={driverStandings.driverStandingsName} className={index % 2 === 0 ? "even_row_drivers" : "odd_row_drivers"}>
                <td>{index + 1}</td>
                <td>
                  {driverStandings.driverStandingsName}
                  </td>
                  <td>{driverStandings.driverCountry}</td>
                  <td>{driverStandings.driverPoints}</td>
              </tr>
              ))}
            </tbody>
        </table>
      </div>
    </div> 
  </>
);
}

export default DriverStandings;