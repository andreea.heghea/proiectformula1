import React, { useRef, useState } from "react";
import emailjs from "@emailjs/browser";
import "../css/GlobalSurvey.css";

function GlobalSurvey() {
    const form = useRef();
    const [username, setUsername]= useState("");
    const [useremail, setUseremail] = useState("");
    const [question1, setQuestion1] = useState("");
    const [question2, setQuestion2] = useState("");
    const [question3, setQuestion3] = useState("");
    const [question4, setQuestion4] = useState("");
    const [question5, setQuestion5] = useState("");
    const [question6, setQuestion6] = useState("");
    const [question7, setQuestion7] = useState("");
    const [question8, setQuestion8] = useState("");
    const [question9, setQuestion9] = useState("");
    const [question10, setQuestion10] = useState("");

    const [usernameError, setUsernameError] = useState(false);
    const [useremailError, setUseremailError] = useState(false);
    const [ question1Error, setQuestion1Error] = useState(false);
    const [ question2Error, setQuestion2Error] = useState(false);
    const [ question3Error, setQuestion3Error] = useState(false);
    const [ question4Error, setQuestion4Error] = useState(false);
    const [ question5Error, setQuestion5Error] = useState(false);
    const [ question6Error, setQuestion6Error] = useState(false);
    const [ question7Error, setQuestion7Error] = useState(false);
    const [ question8Error, setQuestion8Error] = useState(false);
    const [ question9Error, setQuestion9Error] = useState(false);
    const [ question10Error, setQuestion10Error] = useState(false);

  
    const sendEmail = (e) => {
      e.preventDefault();

      if(!username) {
        setUsernameError("Name cannot be null");
      }
      else setUsernameError("");
      if(!useremail) {
        setUseremailError("Email cannot be null");
      }
      if(!question1 || question1.length > 50) {
        setQuestion1Error("Field input must be at least 50 characters or less");
      }
      if(!question2) {
        setQuestion2Error("Field input must be at least 50 characters or less");
      }
      if(!question3) {
        setQuestion3Error("Field input must be at least 50 characters or less");
      }
      if(!question4) {
        setQuestion4Error("Field input must be at least 50 characters or less");
      }
      if(!question5) {
        setQuestion5Error("Field input must be at least 50 characters or less");
      }
      if(!question6) {
        setQuestion6Error("Field input must be at least 50 characters or less");
      }
      if(!question7) {
        setQuestion7Error("Field input must be at least 50 characters or less");
      }
      if(!question8) {
        setQuestion8Error("Field input must be at least 50 characters or less");
      }
      if(!question9) {
        setQuestion9Error("Field input must be at least 50 characters or less");
      }
      if(!question10) {
        setQuestion10Error("Field input must be at least 50 characters or less");
      }
  
      emailjs.sendForm("service_mc0zovk", "template_yy2ge9g", form.current, "J0Cw2QpUH7zzHUHda")
        .then((result) => {
            console.log(result.text);
        }, (error) => {
            console.log(error.text);
        });
        e.target.reset();
    };
     
    return (
      <>
      <div className="feedback_form">
      <form ref={form} onSubmit={sendEmail}>
      <h1 className="header_survey">F1 Global Survey 2023</h1>
        <label value={username}className="label_survey">Please state your name</label>
        <input  type="text" className="feedback_forminput_details" name="user_name" />
        {usernameError && <div className="error_adddriver">{usernameError}</div>}
        <label value={useremail} className="label_survey">Please enter you email adress</label>
        <input type="email" className="feedback_forminput_details" name="user_email" />
        {useremailError && <div className="error_adddriver">{useremailError}</div>}
        <label  value={question1}  className="label_survey">On a scale of 1 to 10 how big of an F1 fan would you call yourself?</label>
        <textarea name="message1" className="textarea"/>
        {question1Error && <div className="error_adddriver">{question1Error}</div>}
        <label value={question2} className="label_survey">How often do you follow F1? Do you watch all three days or just the Sunday race?</label>
        <textarea name="message2" className="textarea"/>
        {question2Error && <div className="error_adddriver">{question2Error}</div>}
        <label value={question3} className="label_survey">Would you say that F1 is the pinnacle of motorsport?</label>
        <textarea  name="message3" className="textarea"/>
        {question3Error && <div className="error_adddriver">{question3Error}</div>}
        <label value={question4} className="label_survey">What is the main reason you watch Formula 1?</label>
        <textarea  name="message4" className="textarea"/>
        {question4Error && <div className="error_adddriver">{question4Error}</div>}
        <label value={question5} className="label_survey">Are you a fan of F1 Sprint races?</label>
        <textarea  name="message5" className="textarea"/>
        {question5Error && <div className="error_adddriver">{question5Error}</div>}
        <label value={question6} className="label_survey">Do you think F1 benefits from having car manufacturers competing in the sport?</label>
        <textarea  name="message6" className="textarea"/>
        {question6Error && <div className="error_adddriver">{question6Error}</div>}
        <label value={question7} className="label_survey">How do you currently access digital F1 news, data or results?</label>
        <textarea   name="message7" className="textarea"/>
        {question7Error && <div className="error_adddriver">{question7Error}</div>}
        <label value={question8} className="label_survey">When watching a Grand Prix do you use a second screen to access live timing through the F1 app or formula1.com website?</label>
        <textarea  name="message8" className="textarea"/>
        {question8Error && <div className="error_adddriver">{question8Error}</div>}
        <label value={question9} className="label_survey">How often do you record coverage of sessions and then watch 'as live' at another time of day?</label>
        <textarea  name="message9" className="textarea"/>
        {question9Error && <div className="error_adddriver">{question9Error}</div>}
        <label value={question10} className="label_survey">In general what type of F1 content would you like to see?</label>
        <textarea  name="message10" className="textarea"/>
        {question10Error && <div className="error_adddriver">{question10Error}</div>}
        <input className="btn_survey" type="submit" value="Send" />
      </form>
      </div>
     </>
    );
 
}

export default GlobalSurvey;