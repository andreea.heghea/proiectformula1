import React, { useState, useEffect } from "react";
import { getDatabase, ref, onValue } from "firebase/database";
import "../css/RaceResults.css";

function RaceResults() {
  const [raceresults, setRaceResults] = useState([]);

  useEffect(() => {
    const db = getDatabase();
    const raceResultsRef = ref(db, "raceResults");

    const unsubscribe = onValue(raceResultsRef, (snapshot) => {
      const raceResultsData = snapshot.val();
      if (raceResultsData) {
        const raceResultsArray = Object.values(raceResultsData);
        setRaceResults(raceResultsArray);
      }
    });

    return () => {
      unsubscribe();
    };
  }, []);
  
  return (
    <>
    <div className="raceresults_page">
      <p>2023 Race Results</p>
      <div className="table_wrapper">
        <table className="raceresults_table">
          <thead>
            <tr>
              <th>Grand Prix</th>
              <th>Date</th>
              <th>Winner</th>
              <th>Winner Constructor</th>
              <th>Laps</th>
              <th>Fastest Lap</th>
            </tr>
          </thead>
          <tbody>
            {raceresults.map((raceresult, index) => (
            <tr className={index % 2 === 0 ? "even_row" : "odd_row"}>
              <td>{raceresult.grandPrix}</td>
              <td>{raceresult.grandPrixDate}</td>
              <td>{raceresult.winner}</td>
              <td>{raceresult.winnerConstructor}</td>
              <td>{raceresult.lapNumber}</td>
              <td>{raceresult.fastestLapOwner}</td>
            </tr>
            ))}
          </tbody>
        </table>
        </div>
    </div> 
  </>
);
}

export default RaceResults;
