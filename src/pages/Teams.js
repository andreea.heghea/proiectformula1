import React, { useState, useEffect } from "react";
import { ref, onValue } from "firebase/database";
import { listAll } from "firebase/storage";
import { storage } from "../firebase/firebase";
import { ref as sRef, getDownloadURL } from "firebase/storage";
import ModalTeam from "../components/ModalTeam";
import { db } from "../firebase/firebase";
import "../css/Teams.css"; 

function Teams() {
  const [teams, setTeams] = useState([]);
  const [openModalTeam, setOpenModalTeam] = useState(false);
  const [selectedTeam, setSelectedTeam] = useState(null);
  const [teamImageUrls, setTeamImageUrls] = useState([]);
  const [imageNames, setImageNames] = useState([]);


  useEffect(() => {
    const countRef = ref(db, "team/");
    onValue(countRef, (snapshot) => {
      const data = snapshot.val();
      if (data) {
        const newPosts = Object.keys(data).map((key) => ({
          id: key,
          ...data[key],
        }));
        setTeams(newPosts);
      } else {
        setTeams([]);
      }
    });

    const imagesRef = sRef(storage, `images/team`);
    listAll(imagesRef)
    .then((res) => {
      const names = res.items.map((item) => item.name);
      setImageNames(names);
      return Promise.all(res.items.map((item) => getDownloadURL(item)));
    })
    .catch((error) => {
      console.log(error);
    })
    .then((urls) => {
      setTeamImageUrls(urls);
    })
    .catch((error) => {
      console.log(error);
    });
    
  }, []);

  const matchImage = () => {
    teams.map((item, index) => {
      teamImageUrls.map((url, index) => {
        if (teamImageUrls[index].includes(item.uid)) {
          item.teamImageUrls = url;
        }
      });
    });
  };
  matchImage();

  const handleTeamClick = (teamName, image) => {
    const selectedTeamObject = teams.find((team) => team.teamname === teamName);
    setSelectedTeam(selectedTeamObject);
  
    setOpenModalTeam(true);
  };
  
  return (
    <div className="teams_page">
      <h2 className="header">Formula 1 Teams List</h2>
      <div className="team_list" 
      onClick={() =>{
        setOpenModalTeam(true);
      }}>
        {teams.map((team) => (
          <li key={team.teamname}  onClick={() => handleTeamClick(team.teamname, team.teamImageUrls) }>
            <div>
            <div className="team_info">
              <div>{team.teamname}</div>
              </div>
            <div className="team_info">
                <div className="label">Base</div>
                <div className="value">{team.base}</div>
              </div>
              <div className="team_info">
                <div className="label">Team Principal</div>
                <div className="value">{team.principalchief}</div>
              </div>
              <div className="team_info">
                <div className="label">Power unit</div>
                <div className="value">{team.powerunit}</div>
              </div>
              <div>
              <button className="openModalTeamBtn" onClick={() => {setOpenModalTeam(true);}}>View more</button>
              </div>
            </div>
          </li>
        ))}
      </div>
      {openModalTeam && <ModalTeam closeModalTeam={() => setOpenModalTeam(false)} selectedTeam={selectedTeam}/>}
    </div>
  );
}

export default Teams;
