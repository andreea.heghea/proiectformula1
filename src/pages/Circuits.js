import React, { useState, useEffect } from "react";
import { ref, onValue } from "firebase/database";
import { listAll } from "firebase/storage";
import { storage } from "../firebase/firebase";
import { ref as sRef, getDownloadURL } from "firebase/storage";
import ModalCircuit from "../components/ModalCircuit";
import { db } from "../firebase/firebase";
import "../css/Circuits.css";

function Circuits() {
  const [circuits, setCircuits] = useState([]);
  const [openModalCircuit, setOpenModalCircuit] = useState(false);
  const [selectedCircuit, setSelectedCircuit] = useState(null);
  const [imageUrls, setImageUrls] = useState([]);
  const [imageNames, setImageNames] = useState([]);
  

  useEffect(() => {
    const countRef = ref(db, "race/");
    onValue(countRef, (snapshot) => {
      const data = snapshot.val();
      if (data) {
        const newPosts = Object.keys(data).map((key) => ({
          id: key,
          ...data[key],
        }));
        setCircuits(newPosts);
      } else {
        setCircuits([]);
      }
    });

    const imagesRef = sRef(storage, `images/circuit`);
    listAll(imagesRef)
    .then((res) => {
      const names = res.items.map((item) => item.name);
      setImageNames(names);
      return Promise.all(res.items.map((item) => getDownloadURL(item)));
    })
    .catch((error) => {
      console.log(error);
    })
    .then((urls) => {
      setImageUrls(urls);
    })
    .catch((error) => {
      console.log(error);
    });
    
  }, []);

  
  const matchImage = () => {
    circuits.map((item, index) => {
      imageUrls.map((url, index) => {
        if (imageNames[index].includes(item.uid)) {
          item.imageUrls = url;
        }
      });
    });
  };
  matchImage();



  const handleCircuitClick = (circuitName, image) => {
    const selectedCircuitObject = circuits.find((race) => race.gpname === circuitName);
    setSelectedCircuit(selectedCircuitObject);
  
    setOpenModalCircuit(true);
  };

  return (
    <div className="circuits_page">
      <h2 className="header">2023 Formula 1 Grand Prix</h2>
      <div className="circuits_list" 
      onClick={() =>{
        setOpenModalCircuit(true);
      }}>
        {circuits.map((race) => (
          <li key={race.gpname}  onClick={() => handleCircuitClick(race.gpname, race.imageUrls) }>
              <div>
              <div className="circuit_info">
              <div>{race.gpname}</div>
              </div>
              <ul>
              </ul>
              <div className="circuit_info">
                <div className="label">First Grand Prix</div>
                <div className="value">{race.gpyear}</div>
              </div>
              <div className="circuit_info">
                <div className="label">No. of laps</div>
                <div className="value">{race.nolaps}</div>
              </div>
              <div className="circuit_info">
                <div className="label">Circuit length</div>
                <div className="value">{race.circuitlength}</div>
              </div>
              <div>
              <button className="openModalCircuitBtn" onClick={() => {setOpenModalCircuit(true);}}>View more</button>
              </div>
              </div>
              </li> 
        ))}
      </div>
      {openModalCircuit && <ModalCircuit closeModalCircuit={() => setOpenModalCircuit(false)} selectedCircuit={selectedCircuit}/>}
    </div>
  );
}

export default Circuits;
