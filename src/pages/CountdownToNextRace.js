import React, { useState, useEffect, useRef } from "react";
import { getDatabase, ref, onValue } from "firebase/database";
import "../css/CountdownToNextRace.css";

function CountdownToNextRace() {
  const [nextRace, setNextRace] = useState("");
  const [countdown, setCountdown] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const intervalid = useRef(null);

  useEffect(() => {
    const db = getDatabase();
    const raceRef = ref(db, "race");
  
    const unsubscribe = onValue(raceRef, (snapshot) => {
      const data = snapshot.val();
      if (data) {
        const currentDate = new Date();
        const races = Object.values(data);
        const upcomingRace = races.filter(race => {
          const raceDate = new Date(race.racedate + "T" + race.racetime);
          return raceDate > currentDate;
        });
  
        if (upcomingRace.length > 0) {
          const nextRaceData = upcomingRace[0];
          setNextRace(nextRaceData);
          console.log(nextRaceData);
  
          const nextRaceDate = new Date(nextRaceData.racedate + "T" + nextRaceData.racetime);

          if (intervalid.current) {
            clearInterval(intervalid.current);
          }

          intervalid.current = setInterval(() => {
            const now = new Date();
            const timeTillRace = nextRaceDate.getTime() - now.getTime();
            setCountdown(timeTillRace);
          }, 1000);
        } else {
          setIsLoading(false);
        }
      }
    });
  
    return () => {
      unsubscribe();
      if (intervalid.current) {
        clearInterval(intervalid.current);
      }
    };
    
  }, []);
  

  const formattimeTillRace = (milliseconds) => {
    const days = Math.floor(milliseconds / (1000 * 60 * 60 * 24));
    const hours = Math.floor((milliseconds % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((milliseconds % (1000 * 60 * 60)) / (1000 * 60));

    return `${days} DAYS ${hours} HRS ${minutes} MIN`;
  };

  return (
    <div className="countdown-container">
      <h2 className="countdown-heading">GRAND PRIX WEEKEND</h2>
      {nextRace && (
        <div className="race-details">
          <p>{nextRace.gpname}</p>
        </div>
      )}
      {isLoading ? (
        <p className="loading-text">Loading...</p>
      ) : countdown !== null ? (
        <p className="countdown-text">{formattimeTillRace(countdown)}</p>
      ) : (
        <p className="loading-text">No upcoming races found.</p>
      )}
    </div>
  );
}

export default CountdownToNextRace;
