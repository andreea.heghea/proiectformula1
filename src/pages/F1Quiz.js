import React, { useState } from "react";
import "../css/F1Quiz.css";

function F1Quiz() {
  const [showFinalResults, setShowFinalResults] = useState(false);
  const [score, setScore] = useState(0);
  const [currentQuestion, setCurrentQuestion] = useState(0);

  const questions = [
    {
      text: "Who won the first-ever Formula 1 World Championship in 1950?",
      options: [
        { id: 0, text: "Juan Manuel Fangio", isCorrect: false },
        { id: 1, text: "Johnnie Parsons", isCorrect: false },
        { id: 2, text: "Giuseppe Farina", isCorrect: true },
        { id: 3, text: "Alberto Ascari", isCorrect: false },
      ],
    },
    {
      text: "Which team has won the most Constructors Championships in Formula 1 history?",
      options: [
        { id: 0, text: "Red Bull", isCorrect: false },
        { id: 1, text: "Ferrari", isCorrect: true },
        { id: 2, text: "Mercedes", isCorrect: false },
        { id: 3, text: "McLaren", isCorrect: false },
      ],
    },
    {
      text: "Who is the youngest-ever Formula 1 World Champion? ",
      options: [
        { id: 0, text: "Sebastian Vettel", isCorrect: true },
        { id: 1, text: "Max Verstappen", isCorrect: false },
        { id: 2, text: "Lewis Hamilton", isCorrect: false },
        { id: 3, text: "Michael Schumacher", isCorrect: false },
      ],
    },
    {
      text: "Which circuit is famous for its street circuit layout and is home to the most famous Grand Prix in the world?",
      options: [
        { id: 0, text: "Jeddah Corniche Circuit", isCorrect: false },
        { id: 1, text: "Baku City Circuit", isCorrect: false },
        { id: 2, text: "Marina Bay Street Circuit", isCorrect: false },
        { id: 3, text: "Circuit de Monaco", isCorrect: true },
      ],
    },
    {
      text: "Which F1 constructor is based in Woking and has won eight Constructors Championships, with the last coming in 1998?",
      options: [
        { id: 0, text: "Renault", isCorrect: false },
        { id: 1, text: "McLaren", isCorrect: true },
        { id: 2, text: "Ferrari", isCorrect: false },
        { id: 3, text: "Williams", isCorrect: false },
      ],
    },
    {
      text: "When did Michael Schumacher make his debut?",
      options: [
        { id: 0, text: "1993", isCorrect: false },
        { id: 1, text: "1990", isCorrect: false },
        { id: 2, text: "1991", isCorrect: true },
        { id: 3, text: "1992", isCorrect: false },
      ],
    },
    {
      text: "Which company is the sole tire supplier in Formula One since 2007?",
      options: [
        { id: 0, text: "Pirelli", isCorrect: true },
        { id: 1, text: "Goodyear", isCorrect: false },
        { id: 2, text: "Michelin", isCorrect: false },
        { id: 3, text: "Bridgestone", isCorrect: false },
      ],
    },
    {
      text: "Where was the first Formula One race held at night?",
      options: [
        { id: 0, text: "Bahrain", isCorrect: false },
        { id: 1, text: "Qatar", isCorrect: false },
        { id: 2, text: "Saudi Arabia", isCorrect: false },
        { id: 3, text: "Singapore", isCorrect: true },
      ],
    },
    {
      text: "Who is the only driver to have won a Formula 1 Grand Prix in a car bearing his own name?",
      options: [
        { id: 0, text: "Enzo Ferrari", isCorrect: false },
        { id: 1, text: "Bruce McLaren", isCorrect: true },
        { id: 2, text: "Niki Lauda", isCorrect: false },
        { id: 3, text: "Frank Williams", isCorrect: false },
      ],
    },
    {
      text: "How many Grand Prix Starts did world champion Ayrton Senna have during his professional career?",
      options: [
        { id: 0, text: "141", isCorrect: false },
        { id: 1, text: "91", isCorrect: false },
        { id: 2, text: "161", isCorrect: true },
        { id: 3, text: "121", isCorrect: false },
      ],
    },
  ];

  const optionClicked = (isCorrect) => {
    if(isCorrect) {
      setScore(score + 1);
    }

    if(currentQuestion + 1 <questions.length){
      setCurrentQuestion(currentQuestion +1);
    } else {
      setShowFinalResults(true);
    }

    
  }

  const restartGame = () => {
    setScore(0);
    setCurrentQuestion(0);
    setShowFinalResults(false);
  }
  
   document.body.style = "background: white";
    return (
      <div>
        <h1 className="quiz_title">Formula 1 Quiz</h1>
        <h2 className="quiz_score">Current score: {score}</h2>
        {showFinalResults ? (
        <div className="final_result">
        <h1 className="final_res_title">Final Result</h1>
        <h2 className="final_res_score">{score} out of {questions.length} correct - ({(score/questions.length) * 100}%)</h2>
          <button onClick={() => restartGame()} className="quiz_btn">Restart Game</button>
      </div>
        ) : (
         <div className="question_card">
         <h2 className="quiz_numberquest">Question {currentQuestion +1 } out of {questions.length}</h2>
         <h3 className="quiz_question">{questions[currentQuestion].text}</h3>
         <ul>
         {questions[currentQuestion].options.map((option) => {
            return (
             <li onClick={() => optionClicked(option.isCorrect)} className="f1_quiz" key={option.id}>{option.text}</li>
          );
        })}
         </ul>
       </div>
        )}
        </div>
    );
 
}

export default F1Quiz;