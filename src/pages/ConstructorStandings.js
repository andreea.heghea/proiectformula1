import React, { useState, useEffect } from "react";
import { getDatabase, ref, onValue, set, query, orderByChild, equalTo, get } from "firebase/database";
import { db } from "../firebase/firebase";
import "../css/ConstructorStandings.css";

function ConstructorStandings() {
  const [constructors, setConstructors] = useState([]);
  const sortedConstructors = [...constructors].sort((a, b) => b.constructorPoints - a.constructorPoints);

  useEffect(() => {
    const db = getDatabase();
    const constructorRef = ref(db, "constructor");

    const unsubscribe = onValue(constructorRef, (snapshot) => {
      const constructorsData = snapshot.val();
      if (constructorsData) {
        const constructorsArray = Object.values(constructorsData);
        setConstructors(constructorsArray);
      }
    });

    return () => {
      unsubscribe();
    };
  }, []);


  const updateConstructorPoints = async (constructorTeamName, updatedPoints) => {
    const constructorRef = ref(db, "constructor");
    const constructorsQuery = query(constructorRef, orderByChild("constructorTeamName"), equalTo(constructorTeamName));
  
    try {
      const snapshot = await get(constructorsQuery);
      if (snapshot.exists()) {
        const constructorKey = Object.keys(snapshot.val())[0];
        const constructorRef = ref(db, `constructor/${constructorKey}/constructorPoints`);
  
        set(constructorRef, updatedPoints)
          .then(() => {
            console.log(`${constructorTeamName} points updated successfully!`);
          })
          .catch((error) => {
            console.error(`Error updating points for ${constructorTeamName}:`, error);
          });
      } else {
        console.log(`Team "${constructorTeamName}" not found in the database.`);
      }
    } catch (error) {
      console.error("Error getting teams:", error);
    }
  };
  
  const constructorNameToBeUpdated = "Aston Martin";
  const updatedPointsValue = 196;
  updateConstructorPoints(constructorNameToBeUpdated, updatedPointsValue);
  
  return (
  <>
    <div className="constructors_page">
      <p>2023 Constructor Standings</p>
      <div className="table_wrapper">
        <table className="constructors_table">
          <thead>
            <tr>
              <th>Position</th>
              <th>Team</th>
              <th>Points</th>
              </tr>
              </thead>
              <tbody>
                {sortedConstructors.map((constructor, index) => (
                <tr key={constructor.constructorTeamName} className={index % 2 === 0 ? "even_row_constructors" : "odd_row_constructors"}>
                  <td>{index + 1}</td>
                  <td>{constructor.constructorTeamName}</td>
                  <td>{constructor.constructorPoints}</td>
                  </tr>
                  ))}
                  </tbody>
                  </table>
                  </div>
                  </div> 
  </>
);
}

export default ConstructorStandings;
