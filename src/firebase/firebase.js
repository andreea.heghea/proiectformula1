import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getDatabase } from "firebase/database";
import { getStorage} from "firebase/storage";


const firebaseConfig = {
  apiKey: "AIzaSyBZO032Dn4wT1pPWHnWTWtBowvX8A8ijn8",
  authDomain: "formula1-191dc.firebaseapp.com",
  projectId: "formula1-191dc",
  storageBucket: "formula1-191dc.appspot.com",
  messagingSenderId: "866061255455",
  appId: "1:866061255455:web:5ca9c1e6d942c34032e62a"
};

const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);
export const db = getDatabase(app);
export const storage = getStorage(app);