import React, { useState, useEffect } from "react";
import Navbar from "./components/Navbar";
import SignIn from "./components/auth/SignIn";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import SignUp from "./components/auth/SignUp";
import Home from "./pages/Home";
import ForgotPassword from "./components/ForgotPassword";
import AddDrivers from "./components/AddDrivers";
import AddTeams from "./components/AddTeams";
import AddRaces from "./components/AddRaces";
import Drivers from "./pages/Drivers";
import Teams from "./pages/Teams";
import AddConstructors from "./components/AddConstructors";
import ConstructorStandings from "./pages/ConstructorStandings";
import DriverStandings from "./pages/DriverStandings";
import AddDriverStandings from "./components/AddDriverStanding";
import AddRaceResults from "./components/AddRaceResults";
import RaceResults from "./pages/RaceResults";
import { auth } from "./firebase/firebase";
import { onAuthStateChanged } from "firebase/auth";
import GlobalSurvey from "./pages/GlobalSurvey";
import F1Quiz from "./pages/F1Quiz";
import Circuits from "./pages/Circuits";

function App() {
  const [authUser, setAuthUser] = useState(null);
  
  useEffect(() => {
    const listen = onAuthStateChanged(auth, (user) => {
        if(user) {
          setAuthUser(user)
        } else {
          setAuthUser(null);
        }
    });
 
    return () => {
      listen();
    };
  }, []);
  return(
  <Router>
  <>
      <div className="App">
      {authUser ? <Navbar /> : 
      <Routes>
        <Route>
        <Route path="/*" element={<SignIn />}/>
        <Route path="/signup" element={<SignUp />}/>
        <Route path="/forgotpassword" element={<ForgotPassword />}/>
        </Route>
      </Routes>
      }
    </div>
    <Routes>
    <Route path="/adddrivers" element={<AddDrivers />}/>
    <Route path="/addteams" element={<AddTeams />}/>
    <Route path="/addraces" element={<AddRaces />}/>
    <Route path="addconstructorstandings" element={<AddConstructors />}/>
    <Route path="/constructorstandings" element={<ConstructorStandings />}/>
    <Route path="/adddriverstandings" element={<AddDriverStandings />}/>
    <Route path="/driverstandings" element={<DriverStandings />}/>
    <Route path="/addraceresults" element={<AddRaceResults />}/>
    <Route path="/raceresults" element={<RaceResults />}/>
    <Route path="/drivers" element={<Drivers />}/>
    <Route path="/teams" element={<Teams />}/>
    <Route path="/home" element={<Home />}/>
    <Route path="/survey" element={<GlobalSurvey />}></Route>
    <Route path="/quiz" element={<F1Quiz />}></Route>
    <Route path="/circuits" element={<Circuits />}></Route>
    </Routes>
  </>
  </Router>
  );
}

export default App;

